from django.utils.translation import ugettext as _

import horizon

"""
Display all panels in a grouped view.
"""
class BasePanels(horizon.PanelGroup):
    slug = "greennova"
    name = _("Greennova")
    panels = ('greennodes', 'greenconfs',)


class GreennovaPlugin(horizon.Dashboard):
    name = _("Greennova")
    slug = "greennova_dashboard"
    """
    Note: the comma at the end DOES make a difference -
    without it, the last PanelGroup is not displayed at all!
    """
    panels = (BasePanels,)
    default_panel = 'greennodes'
    """
    Only visible and accessible for users with admin permissions.
    """
    roles = ('admin',)
    nav = False

horizon.register(GreennovaPlugin)
