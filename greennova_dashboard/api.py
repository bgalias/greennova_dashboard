"""
Horizon API-Layer. Contains methods for RESTful access to the nova-API.
Uses novaclient for simple HTTP methods.
"""

from horizon.api import novaclient

"""
Fetches a list of all Green Nova nodestates.
"""
def greennodes_list(request):
    nodes = novaclient(request).greennodes.nodes_list()
    return nodes

"""
Update a Green Nova node enabled-status.
"""
def greennode_update(request, greennode_id, body):
    response = novaclient(request).greennodes.node_update(greennode_id, body)
    if response is None:
        return True
    else:
        return response

"""
Fetches a list of all Green Nova configuration parameters.
"""
def greenconfs_list(request):
    confs = novaclient(request).greenconfs.confs_list()
    return confs

"""
Fetches a single Green Nova config parameter by key.
"""
def greenconf_get(request, greenconf_id):
    conf = novaclient(request).greenconfs.conf_get(greenconf_id)
    return conf


"""
Update a Greennova config parameter.
"""
def greenconf_update(request, greenconf_id, body):
    response = novaclient(request).greenconfs.conf_update(greenconf_id, body)
    if response is None:
        return True
    else:
        return response
