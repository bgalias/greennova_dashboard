"""
Panel module handles the horizon registration.
"""

from django.utils.translation import ugettext_lazy as _

import horizon
import greennova_dashboard.dashboard

class GreenNodes(horizon.Panel):
    name = _("Node Status")
    slug = 'greennodes'


greennova_dashboard.dashboard.GreennovaPlugin.register(GreenNodes)