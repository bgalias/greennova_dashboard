"""
URL mappings for django.
"""

from django.conf.urls.defaults import patterns, url
from .views import GreenNodesIndexView

urlpatterns = patterns('greennova_dashboard.greennodes.views',
    url(r'^$', GreenNodesIndexView.as_view(), name='index'))
