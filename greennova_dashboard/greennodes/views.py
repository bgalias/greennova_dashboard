"""
Greennodes view. Handles the specific
configuration of templates
"""

import logging

from horizon import tables
from horizon import exceptions
from .tables import GreenNodesInstancesTable
from greennova_dashboard import api

LOG = logging.getLogger(__name__)


class GreenNodesIndexView(tables.DataTableView):
    table_class = GreenNodesInstancesTable
    template_name = 'greennova_dashboard/greennodes/index.html'

    """
    Called by the framework to load data into the table.
    """
    def get_data(self):
        request = self.request
        nodes = []

        try:
            nodes = api.greennodes_list(request)
        except:
            exceptions.handle(request, _("Unable to retrieve greennodes."))

        return nodes
