"""
Table module with column configuration and
table actions.
"""

import logging

from django.utils.translation import ugettext_lazy as _
from horizon import tables
from django import shortcuts
from django.contrib import messages
from greennova_dashboard import api

LOG = logging.getLogger(__name__)


"""
Enable a nodes shutdown ability.
"""
class EnableGreennodesAction(tables.Action):
    name = "enable"
    verbose_name = _("Enable")
    verbose_name_plural = _("Enable Greennodes")
    classes = ("btn-enable",)

    def allowed(self, request, greennode):
        if greennode.node_shutdown_allowed == "disabled":
            return True
        else:
            return False

    """
    Handle the button action from the table row.
    """
    def handle(self, data_table, request, object_ids):
        failures = 0
        enabled = []
        for obj_id in object_ids:
            try:
                body = {"node_shutdown_allowed": "enabled"}
                api.greennode_update(request, obj_id, body)
                enabled.append(obj_id)
            except Exception, e:
                failures += 1
                messages.error(request, _("Error enabling shutdown of node: %s") % e)
                LOG.exception("Error enabling shutdown.")
        if failures:
            messages.info(request, _("Enabled shutdown of node: %s")
                                     % ", ".join(enabled))
        else:
            messages.success(request, _("Successfully enabled shutdown of node: %s")
                                        % ", ".join(enabled))
        """
        Redirect to the table view.
        """
        return shortcuts.redirect('horizon:greennova_dashboard:greennodes:index')


"""
Disable a nodes shutdown ability.
"""
class DisableGreennodesAction(tables.Action):
    name = "disable"
    verbose_name = _("Disable")
    verbose_name_plural = _("Disable Greennodes")
    classes = ("btn-disable",)

    def allowed(self, request, greennode):
        if greennode.node_shutdown_allowed == "enabled":
            return True
        else:
            return False
    
    """
    Handle the button action from the table row.
    """
    def handle(self, data_table, request, object_ids):
        failures = 0
        disabled = []
        for obj_id in object_ids:
            try:
                body = {"node_shutdown_allowed": "disabled"}
                api.greennode_update(request, obj_id, body)
                disabled.append(obj_id)
            except Exception, e:
                failures += 1
                messages.error(request, _("Error disabling shutdown of node: %s") % e)
                LOG.exception("Error disabling user.")
        if failures:
            messages.info(request, _("Disabled shutdown of node: %s")
                                     % ", ".join(disabled))
        else:
            if disabled:
                messages.success(request, _("Successfully disabled shutdown of node: %s")
                                            % ", ".join(disabled))
        """
        Redirect to the table view.
        """
        return shortcuts.redirect('horizon:greennova_dashboard:greennodes:index')


"""
Metadata for the table:
- column names
- row actions
- verbose name
"""
class GreenNodesInstancesTable(tables.DataTable):
    GN_ENABLED_CHOICES = (("enabled", True), ("disabled", False))

    node_id = tables.Column("node_id", verbose_name=_("Node ID"))
    name = tables.Column("name", verbose_name=_("Hostname"))
    node_mac = tables.Column("node_mac", verbose_name=_("MAC-Address"))
    node_state = tables.Column("node_state", verbose_name=_("Status"))
    node_last_change = tables.Column("node_last_change",
                                     verbose_name=_("Last Change"))
    node_shutdown_allowed = tables.Column("node_shutdown_allowed",
                                          verbose_name=_("Shutdown allowed?"),
                                          status=True,
                                          status_choices=GN_ENABLED_CHOICES)

    class Meta:
        name = "cnodes"
        verbose_name = _("Compute Nodes")
        row_actions = (EnableGreennodesAction, DisableGreennodesAction)
