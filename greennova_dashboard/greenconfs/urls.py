"""
URL mappings for django.
In addition to the main view, a mapping
for updating parameters is added.
"""

from django.conf.urls.defaults import patterns, url
from .views import GreenconfsIndexView, GreenconfsUpdateView

urlpatterns = patterns('greennova_dashboard.greenconfs.views',
    url(r'^$', GreenconfsIndexView.as_view(), name='index'),
    url(r'^(?P<greenconf_id>[^/]+)/update/$',
        GreenconfsUpdateView.as_view(), name='update'))
