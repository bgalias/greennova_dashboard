"""
Greenconfs view. Handles the specific
configuration of templates
"""

from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from horizon import exceptions
from horizon import forms
from horizon import tables
from greennova_dashboard import api

from .forms import UpdateGreenconfForm
from .tables import GreenconfsTable


class GreenconfsIndexView(tables.DataTableView):
    table_class = GreenconfsTable
    template_name = 'greennova_dashboard/greenconfs/index.html'

    """
    Called by the framework to load data into the table.
    """
    def get_data(self):
        request = self.request
        confs = []
        try:
            confs = api.greenconfs_list(request)
        except:
            exceptions.handle(request, _("Unable to retrieve greenconfs."))
        return confs


"""
Update class is used when displaying the update form.
"""
class GreenconfsUpdateView(forms.ModalFormView):
    form_class = UpdateGreenconfForm
    template_name = 'greennova_dashboard/greenconfs/update.html'
    context_object_name = 'greenconf'

    """
    Retrieve the selected row from the database.
    """
    def get_object(self, *args, **kwargs):
        greenconf_id = self.kwargs['greenconf_id']
        try:
            return api.greenconf_get(self.request, greenconf_id)
        except:
            redirect = reverse("greennova_dashboard:greenconfs:index")
            exceptions.handle(self.request,
                              _('Unable to update greenconf.'),
                              redirect=redirect)

    def get_context_data(self, **kwargs):
        context = super(GreenconfsUpdateView, self).get_context_data(**kwargs)
        context['greenconf'] = self.get_object()
        return context

    def get_initial(self):
        return {'id': self.object.id,
                'key': getattr(self.object, 'key', None),
                'value': getattr(self.object, 'value', None),
                'description': getattr(self.object, 'description', None)}
        
