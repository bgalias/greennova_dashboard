"""
Table module with column configuration and
table actions.
"""

import logging

from django.utils.translation import ugettext_lazy as _

from horizon import tables


LOG = logging.getLogger(__name__)


"""
Edit a configuration parameter.
Opens a new form.
"""
class EditGreenconfLink(tables.LinkAction):
    name = "edit"
    verbose_name = _("Edit")
    url = "horizon:greennova_dashboard:greenconfs:update"
    classes = ("ajax-modal", "btn-edit")


"""
Metadata for the table:
- column names
- row actions
- verbose name
"""
class GreenconfsTable(tables.DataTable):
    key = tables.Column('key', verbose_name=_('Key'))
    value = tables.Column('value', verbose_name=_('Value'))
    description = tables.Column('description', verbose_name=_('Description'))

    class Meta:
        name = "greenconfs"
        verbose_name = _("Green Nova Configurations")
        row_actions = (EditGreenconfLink,)
