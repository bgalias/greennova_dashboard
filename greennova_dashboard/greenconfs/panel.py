"""
Panel module handles the horizon registration.
"""

from django.utils.translation import ugettext_lazy as _

import horizon
import greennova_dashboard.dashboard


class Greenconfs(horizon.Panel):
    name = _("Configuration")
    slug = 'greenconfs'


greennova_dashboard.dashboard.GreennovaPlugin.register(Greenconfs)