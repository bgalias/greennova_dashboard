"""
Forms module handles the update form
when changing a configuration parameter.
"""

import logging

from django.utils.translation import ugettext_lazy as _

from horizon import exceptions
from horizon import forms
from django.contrib import messages
from django import shortcuts
from greennova_dashboard import api


LOG = logging.getLogger(__name__)


class BaseGreenconfForm(forms.SelfHandlingForm):
    def __init__(self, request, *args, **kwargs):
        super(BaseGreenconfForm, self).__init__(*args, **kwargs)

    @classmethod
    def _instantiate(cls, request, *args, **kwargs):
        return cls(request, *args, **kwargs)

    def clean(self):
        data = super(forms.Form, self).clean()
        return data


"""
update form with hidden fields.
"""
class UpdateGreenconfForm(BaseGreenconfForm):
    attr = {'readonly': 'readonly'}
    id = forms.CharField(label=_("id"), widget=forms.HiddenInput)
    key = forms.CharField(label=_("key"),
                          widget=forms.TextInput(attrs=attr))
    value = forms.CharField(label=_("value"))
    description = forms.CharField(label=_("Description"))

    """
    Handle the submit action from the form.
    """
    def handle(self, request, data):
        greenconf_id = data.pop('id')
        try:
            api.greenconf_update(request, greenconf_id, data)
            messages.success(request,
                             _('Configuration has been updated successfully.'))
        except:
            exceptions.handle(request, ignore=True)
        return shortcuts.redirect('horizon:greennova_dashboard:greenconfs:index')
