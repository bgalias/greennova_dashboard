from novaclient import base
from novaclient.exceptions import NotFound


class Greennode(base.Resource):

    def __repr__(self):
        return "<Greennode: %s>" % self.name


class Greenconf(base.Resource):

    def __repr__(self):
        return "<Greenconf: %s>" % self.key


class GreennodesManager(base.Manager):

    resource_class = Greennode

    def nodes_list(self):
        nodes = []
        try:
            nodes = self._list("/os-greennodes", "greennodes")
        except NotFound:
            pass

        return nodes

    def node_update(self, greennode_id, body):
        result = self._update("/os-greennodes/%s" % greennode_id, body)
        return Greennode(self, result)


class GreenconfsManager(base.Manager):

    resource_class = Greenconf

    def confs_list(self):
        confs = []
        try:
            confs = self._list("/os-greenconfs", "greenconfs")
        except NotFound:
            pass

        return confs

    def conf_get(self, greenconf_id):
        return self._get("/os-greenconfs/%s" % greenconf_id, "greenconf")

    def conf_update(self, greenconf_id, body):
        result = self._update("/os-greenconfs/%s" % greenconf_id, body)
        return Greenconf(self, result)
